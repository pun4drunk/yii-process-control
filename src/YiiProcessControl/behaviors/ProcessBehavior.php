<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessBehavior
 *
 * @author vladislav
 */
namespace YiiProcessControl\behaviors;
use \YiiComponents\behaviors\CacheBehavior;
use \YiiComponents\behaviors\DependentBehavior;

class ProcessBehavior extends DependentBehavior {
    
    public $name;
    public $commandName;
    public $maxCnt = 1;
    public $cacheKeyPrefix = 'app.pcntl';
    public $category = 'process';
    
    public $pids, $pidsStale;
        
    public function dependencies() {
        return array(
            'logger' => 'YiiComponents\behaviors\LoggerBehavior',
        );
    }
    
    public function attach($owner) {
        
        parent::attach($owner);
        
        $this->attachBehavior('cache', array(
            'class' => 'YiiComponents\behaviors\CacheBehavior',
            'cacheKeyPrefix' => $this->cacheKeyPrefix,
            'cacheExpire' => 0,
            'cacheRequired' => true,
        ));
        
        $this->init();
            
    }
    
    protected function load() {
        if (!$saved = $this->cache->get($this->name)) {
            $saved = array();
        }
        $this->pids = $saved;
    }
    
    public function update() {
        return $this->init(true);
    }
    
    protected function init($update = false) {
        if (is_null($this->commandName)) {
            throw new \CException("Command name must be set before invoking ".get_class($this).'::getPids()');
        }

        $this->load();
        $saved = $this->pids;

        $search = <<<EOP
ps -ef | grep "$this->commandName"| grep -v grep | awk '{print $2}';
EOP;
        $this->logger->addTrace("searching: /$search/");
        $result = trim( `$search` );
        $pids = array_filter(explode( PHP_EOL, $result ));
        $this->pids = $pids;
        
        if ($unregistered = array_diff($pids, $saved)) {
            $pidsString = $this->getPidsString($unregistered);
            if (!$update) $this->logger->addWarning("Unregistered pids: $this->name[$pidsString]", $this->category);
        }

        if ($stale = array_diff($saved, $pids)) {
            $this->pidsStale = $pids;
            $pidsString = $this->getPidsString($stale);
            if (!$update) $this->logger->addWarning("Stale pids: $this->name[$pidsString]", $this->category);
        }
        
        if ($unregistered || $stale) {
            $this->save();
        }
    }
    
    protected function save() {
        $this->cache->set($this->name, $this->pids);
    }
    
    public function getPidsCount() {
        return count($this->pids);
    }
    
    public function getPidsString($pids = NULL, $glue = " ") {
        $pids = is_null($pids) ? $this->pids : $pids;
        return implode($glue, $pids);
    }
    
    protected function checkPid($pid) {
        $pids = $this->ps();
        return in_array($pid, $pids);
    }
    
    
    protected function updatePids() {
        $this->cache->set($this->name, $this->pids);
    }
    
    
    
}
