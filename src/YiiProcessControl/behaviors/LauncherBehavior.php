<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of LauncherBehavior
 *
 * @author vladislav
 */
namespace YiiProcessControl\behaviors;
use \YiiComponents\behaviors\DependentBehavior;
use YiiComponents\helpers\ConsoleHelper;

class LauncherBehavior extends DependentBehavior {
    
    public $yiicDir;
    public $format;
    public $expression;
    public $args = array();
    
    public function prepare($name, $action, $args) {
        $this->expression = $this->getExpression($name, $action, $args);
        return $this;
    }
    
    public function execute($expression = NULL) {
        
        $expression = is_null($expression) ? $this->expression : $expression;
        if (!$expression) {
            throw new \CException("There is nothing to execute");
        }
        return exec($expression);
    }
    
    public function getExpression($name, $action, $args = array()) {
        
        $result = $this->yiicDir.'/'.ConsoleHelper::getExpression($name, $action, $args);
        
        if ($this->format) {
            $result = vsprintf($this->format, array_merge(array($result), $this->args));
        }
        
        return $this->expression = $result;
    }
    
}
