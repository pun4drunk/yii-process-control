<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of ProcessControlBehavior
 *
 * @author vladislav
 */
namespace YiiProcessControl\behaviors;
use YiiProcessControl\behaviors\ProcessBehavior;

class ProcessControlBehavior extends ProcessBehavior {
    
    public $callback;
    public $callbackArgs = array();
    public $pid;
    
    protected function init($update = false) {
        $this->pid = getmypid();
        $this->callback = is_null($this->callback) ? array($this, 'abort') : $this->callback;
        
        parent::init($update);
        
        $this->lock();
    }
    
    protected function load() {
        parent::load();
        array_push($this->pids, $this->pid);
    }
    
    public function end() {
        $this->unlock();
        return $this;
    }
    
    protected function abort() {
        exit;
    }
    
    protected function lock() {
        
        if ($this->getPidsCount() > $this->maxCnt) {
            array_pop($this->pids);
            $this->logger->addInfo("Process limit exceeded: $this->maxCnt ".json_encode($this->pids), $this->category);
            return call_user_func_array($this->callback, $this->callbackArgs);
        }
        $key = array_search($this->pid, $this->pids);
        
        $this->logger->addTrace("Current pid for $this->name[$key]: $this->pid", $this->category);
        return $this;
        
    }
    
    protected function unlock() {
        
        if ($this->pid && $this->pids) {
            $key = array_search($this->pid, $this->pids);
            if (false !== $key) {
                $this->logger->addTrace("Removing current pid for $this->name[".($key + 1)."]: $this->pid", $this->category);
                unset($this->pids[$key]);
                $this->save();
            }
        }
        
        return $this;
        
    }
    
}
